"""Author: Joanna Kus
 This class deals with information about a card for use with card games.
 Used with Hands superclass (Hand.py). 
"""
from Enums.Rank import *
from Enums.Suit import *
from Enums.Visibility import *

class Card(object):
    """CLASS: Stores information about a given card. Stores a suit and rank value
    and a visual representation of the card. Read-only object.

    METHODS:
        >> __init__(self, suit=None, rank=None, visibility=Visibility.VISIBLE)
            Initializes the card object.

        >> __str__(self)
            Outputs rank and suit for the card.

        >> suit(self)
            Gets card suit.

        >> rank(self)
            Gets card rank.

        >> isvisible(self)
            Gets card visibility.

        >> isvisible(self, visibility)
            Sets card visibility.
            
        >> top_img(self)
            Gets top card image file URL.
            
        >> side_img(self)
            Gets side card image file URL.
            
        >> full_img(self)
            Gets full card image file URL.

        >> compare_cards_by_rank(self, other_card)
            Compares two cards by rank. Useful for sorting.

        >> compare_cards_by_suit(self, other_card)
            Compares two cards by suit (sorted in alphabetical order). Useful for sorting.

        >> compare_cards(self, other_card)
            Compares two cards by suit and then by rank. Useful for sorting.

        >> is_same_as(self, other_card)
            Compares two cards. Checks to see if the two cards are the same.
    """

    def __init__(self, rank=None, suit=None, isvisible=Visibility.VISIBLE, top_img=None, side_img=None, full_img=None):
        """Creates a Card object.

        PARAMETERS:
            suit (string):                Suit of the given card (e.g. Spades, Hearts, etc.).
            rank (string):                Rank of the given card (e.g. A, J, 2, 9, etc.).
            visibility (Visibility enum): Visibility of the card.
            top_img(string):              Top image file URL. 
            side_img(string):             Side image file URL.
            full_img(string):             Full image file URL.
        """
        
        imgs = {"11": "https://upload.wikimedia.org/wikipedia/commons/6/61/Ace_of_clubs.svg", 
        "12": "https://upload.wikimedia.org/wikipedia/commons/4/42/2_of_clubs.svg",
        "13": "https://upload.wikimedia.org/wikipedia/commons/4/4d/3_of_clubs.svg", 
        "14": "https://upload.wikimedia.org/wikipedia/commons/e/e5/4_of_clubs.svg", 
        "15": "https://upload.wikimedia.org/wikipedia/commons/7/72/5_of_clubs.svg",
        "16": "https://upload.wikimedia.org/wikipedia/commons/4/49/6_of_clubs.svg",
        "17": "https://upload.wikimedia.org/wikipedia/commons/d/db/7_of_clubs.svg",
        "18": "https://upload.wikimedia.org/wikipedia/commons/5/54/8_of_clubs.svg", 
        "19": "https://upload.wikimedia.org/wikipedia/commons/6/67/9_of_clubs.svg", 
        "110": "https://upload.wikimedia.org/wikipedia/commons/c/c9/10_of_clubs.svg",
        "111": "https://upload.wikimedia.org/wikipedia/commons/7/7b/Jack_of_clubs2.svg",
        "112": "https://upload.wikimedia.org/wikipedia/commons/d/d2/Queen_of_clubs2.svg",
        "113": "https://upload.wikimedia.org/wikipedia/commons/d/d3/King_of_clubs2.svg",
        "114": "https://upload.wikimedia.org/wikipedia/commons/6/61/Ace_of_clubs.svg",
        "21": "https://upload.wikimedia.org/wikipedia/commons/e/e6/Ace_of_diamonds.svg", 
        "22": "https://upload.wikimedia.org/wikipedia/commons/8/8d/2_of_diamonds.svg",
        "23": "https://upload.wikimedia.org/wikipedia/commons/5/50/3_of_diamonds.svg",
        "24": "https://upload.wikimedia.org/wikipedia/commons/b/b8/4_of_diamonds.svg",
        "25": "https://upload.wikimedia.org/wikipedia/commons/6/6b/5_of_diamonds.svg",
        "26": "https://upload.wikimedia.org/wikipedia/commons/3/34/6_of_diamonds.svg",
        "27": "https://upload.wikimedia.org/wikipedia/commons/8/83/7_of_diamonds.svg",
        "28": "https://upload.wikimedia.org/wikipedia/commons/5/5a/8_of_diamonds.svg",
        "29": "https://upload.wikimedia.org/wikipedia/commons/f/f2/9_of_diamonds.svg",
        "210": "https://upload.wikimedia.org/wikipedia/commons/f/f3/10_of_diamonds.svg",
        "211": "https://upload.wikimedia.org/wikipedia/commons/8/80/Jack_of_diamonds2.svg",
        "212": "https://upload.wikimedia.org/wikipedia/commons/d/d9/Queen_of_diamonds2.svg",
        "213": "https://upload.wikimedia.org/wikipedia/commons/c/c6/King_of_diamonds2.svg",
        "214": "https://upload.wikimedia.org/wikipedia/commons/e/e6/Ace_of_diamonds.svg",
        "31": "https://upload.wikimedia.org/wikipedia/commons/5/5a/Ace_of_spades.svg", 
        "32": "https://upload.wikimedia.org/wikipedia/commons/a/a4/2_of_spades.svg",
        "33": "https://upload.wikimedia.org/wikipedia/commons/e/eb/3_of_spades.svg",
        "34": "https://upload.wikimedia.org/wikipedia/commons/a/a4/4_of_spades.svg",
        "35": "https://upload.wikimedia.org/wikipedia/commons/8/8a/5_of_spades.svg",
        "36": "https://upload.wikimedia.org/wikipedia/commons/4/4e/6_of_spades.svg",
        "37": "https://upload.wikimedia.org/wikipedia/commons/f/f7/7_of_spades.svg",
        "38": "https://upload.wikimedia.org/wikipedia/commons/4/40/8_of_spades.svg",
        "39": "https://upload.wikimedia.org/wikipedia/commons/6/63/9_of_spades.svg",
        "310": "https://upload.wikimedia.org/wikipedia/commons/6/68/10_of_spades.svg",
        "311": "https://upload.wikimedia.org/wikipedia/commons/a/a9/Jack_of_spades2.svg",
        "312": "https://upload.wikimedia.org/wikipedia/commons/d/d2/Queen_of_spades2.svg",
        "313": "https://upload.wikimedia.org/wikipedia/commons/2/22/King_of_spades2.svg",
        "314": "https://upload.wikimedia.org/wikipedia/commons/5/5a/Ace_of_spades.svg",
        "41": "https://upload.wikimedia.org/wikipedia/commons/0/07/Ace_of_hearts.svg", 
        "42": "https://upload.wikimedia.org/wikipedia/commons/3/39/2_of_hearts.svg",
        "43": "https://upload.wikimedia.org/wikipedia/commons/5/5d/3_of_hearts.svg",
        "44": "https://upload.wikimedia.org/wikipedia/commons/e/e9/4_of_hearts.svg",
        "45": "https://upload.wikimedia.org/wikipedia/commons/a/a1/5_of_hearts.svg",
        "46": "https://upload.wikimedia.org/wikipedia/commons/7/7e/6_of_hearts.svg",
        "47": "https://upload.wikimedia.org/wikipedia/commons/4/4a/7_of_hearts.svg",
        "48": "https://upload.wikimedia.org/wikipedia/commons/6/6b/8_of_hearts.svg",
        "49": "https://upload.wikimedia.org/wikipedia/commons/9/9d/9_of_hearts.svg",
        "410": "https://upload.wikimedia.org/wikipedia/commons/a/ad/10_of_hearts.svg",
        "411": "https://upload.wikimedia.org/wikipedia/commons/3/34/Jack_of_hearts2.svg",
        "412": "https://upload.wikimedia.org/wikipedia/commons/f/f6/Queen_of_hearts2.svg",
        "413": "https://upload.wikimedia.org/wikipedia/commons/0/06/King_of_hearts2.svg",
        "414": "https://upload.wikimedia.org/wikipedia/commons/0/07/Ace_of_hearts.svg"}
        
        self._suit = suit
        self._rank = rank
        self._isvisible = isvisible
        self._top_img = top_img
        self._side_img = side_img
        self._full_img = full_img

        if suit != None:
            self._suit = suit
        if rank != None:
            self._rank = rank
        if isvisible == None:
            self._isvisible = Visibility.VISIBLE

        if type(isvisible) != Visibility:
            raise TypeError("Incorrect data type for visibility parameter; must be Visibility enum.")

        if suit != None and rank != None:    
            self._full_img = imgs[str(self.suit.value) + str(self.rank.value)]

    def __str__(self):
        """METHOD: Outputs rank and suit for the card.

            >> __str__(self)

            RETURNS:
                string: Card rank and suit.
        """
        new_string = str(self._rank) + str(self._suit)
        return new_string

    @property
    def suit(self):
        """METHOD: Gets card suit.

            >> suit(self)

            RETURNS:
                Suit enum: Card suit information.
        """
        return self._suit

    @property
    def rank(self):
        """METHOD: Gets card rank.

            >> rank(self)

            RETURNS:
                Rank enum: Card rank information.
        """
        return self._rank

    @property
    def isvisible(self):
        """METHOD: Gets card visibility.

            >> isvisible(self)

            RETURNS:
                Visibility enum: Card visibility.
        """
        return self._isvisible

    @isvisible.setter
    def isvisible(self, isvisible):
        """METHOD: Sets card visibility.

            >> isvisible(self, isvisible)

            PARAMETERS:
                isvisible (Visibility enum): Card visibility.
        """
        if type(isvisible) == Visibility:
            self._isvisible = isvisible

    @property
    def top_img(self):
        """METHOD: Gets top card image file URL.
        
            >> top_img(self)
            
            RETURNS:
                string: Image URL.
        """
        return self._top_img
    
    @property
    def side_img(self):
        """METHOD: Gets side card image file URL.

            >> side_img(self)

            RETURNS:
                string: Image URL.
        """
        return self._side_img
        
    @property
    def full_img(self):
       	"""METHOD: Gets full card image file URL.
 
            >> full_img(self)

            RETURNS:
                string: Image URL.
        """
       	return self._full_img

    def compare_cards_by_rank(self, other_card):
        """METHOD: Compares two cards by rank. Useful for sorting.

            >> compare_cards_by_rank(self, other_card)

            PARAMETERS:
                other_card (Card object): Other card to compare to.

            RETURNS:
                int: Negative integer if this card is less than the other card.
                     Zero if this card is equal to the other card.
                     Positive integer if this card is greater than the other card.
        """
        comparison = -1
        rankself = 0
        rankother = 0

        rankself = int(self._rank.value)
        rankother = int(other_card._rank.value)
        
        if rankself == rankother:
            comparison = 0
        elif rankself > rankother:
            comparison = 1
        elif rankself < rankother:
            comparison = -1

        return comparison

    def compare_cards_by_suit(self, other_card):
        """METHOD: Compares two cards by suit (sorted in alphabetical order). Useful for sorting.

            >> compare_cards_by_suit(self, other_card)

            PARAMETERS:
                other_card (Card object): Other card to compare to.

            RETURNS:
                int: Negative integer if this card is less than the other card.
                     Zero if this card is equal to the other card.
                     Positive integer if this card is greater than the other card.
        """
        comparison = -1
        suitself = 0
        suitother = 0

        suitself = self._suit.value
        suitother = other_card._suit.value
        
        if suitself == suitother:
            comparison = 0
        elif suitself > suitother:
            comparison = 1

        return comparison

    def compare_cards(self, other_card):
        """METHOD: Compares two cards. Useful for sorting. Cards are sorted by suit
        and then by rank.

            >> compare_cards(self, other_card)

            PARAMETERS:
                other_card (Card object): Other card to compare to.

            RETURNS:
                int: Negative integer if this card is less than the other card.
                     Zero if this card is equal to the other card.
                     Positive integer if this card is greater than the other card.
        """
        comparison = -1

        if self.compare_cards_by_suit(other_card) == -1:
            comparison = -1
        elif self.compare_cards_by_suit(other_card) == 0:
            comparison = self.compare_cards_by_rank(other_card)
        else:
            comparison = 1
        
        return comparison

    def is_same_as(self, other_card):
        """METHOD: Compares two cards. Checks to see if the two cards are the same.

            >> is_same_as(self, other_card)

            PARAMETERS:
                other_card (Card object): Other card to compare to.

            RETURNS:
                bool: True if the two cards are of the same value.
                      False otherwise.
        """
        if self.compare_cards(other_card) == 0:
            return True
        else:
            return False

def main():
    card = Card(rank=Rank.THREE, suit=Suit.CLUBS)
    print(card)

if __name__ == '__main__':
    main()
