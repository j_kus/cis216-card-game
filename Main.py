from Game import *
from Enums.Visibility import *
from Enums.Rank import *

def main(): 
    """Runs main program logic"""
    game = Game(hand_size = 5, hand_count = 4, max_hand_size = 52, discard_type = Visibility.INVISIBLE, ace_rank = Rank.ACELOW)
    game.control_players()
    
if __name__ == "__main__":
    main()