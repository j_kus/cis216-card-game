"""This program takes a user's weight and height in US/Imperial
   and calculates their BMI

Input:
    Height in US/Imperial scale
    Weight in US/Imperial scale

Output:
    Body Mass Index
    BMI ranges key

Example:
    Input Height in US/Imperial scale:
    6/1
    Input Weight in US/Imperial scale:
    170
    Your bmi is in normal range at 22.43

References:
    https://en.wikiversity.org/wiki/Applied_Programming/Variables/Python3
    https://en.wikipedia.org/wiki/Body_mass_index
    http://www.mathsisfun.com/metric-imperial-conversion-charts.html
"""
import os
import tkinter.scrolledtext
from tkinter import *
root = None
scrolledtext = None


def create_root():
    """Creates top-level window.

    Args:
        None

    Returns:
        None

    Modifies:
        global root

    """
    global root

    root = Tk()
    root.title("BMI Calculator")
    root.geometry("%dx%d+0+0" % root.maxsize())
    frame = Frame(root)
    frame.pack()
    height = Entry(frame, bd=5)
    height.pack()
    h_l = Label(frame, text="Height in f/i")
    h_l.pack()
    weight = Entry(frame, bd=5)
    weight.pack()
    w_l = Label(frame, text="Weight in LBS")
    w_l.pack()
    b1 = Button(frame, text="Submit", command=lambda: task_manager(height, weight))
    b1.pack(side=LEFT)
    b2 = Button(frame, text="Quit", command=quit)
    b2.pack(side=RIGHT)
    add_textbox("Welcome to the BMI calculator.")
    scrolledtext.insert(INSERT, """\n\nBody mass index key: \nUnderweight:BMI<18.6 \nNormal Weight:18.5>=BMI>=25
Overweight:25>=BMI>=30\nObese:\tBMI >30\n\n""")


def quit():
    """Exits the program

    Args:
        None

    Returns:
        None

    Raises:
        None
    """
    os._exit(0)


def add_textbox(text):
    """Adds the scrolled textbox to the top-level window.

    Args:
        None

    Returns:
        None

    Modifies:
        global root

    """
    global root, scrolledtext

    scrolledtext = tkinter.scrolledtext.ScrolledText(root)
    scrolledtext.pack(fill="both", expand=True)

    text_out = text

    scrolledtext.insert(INSERT, text)


def task_manager(height, weight):
    """This function supports the interaction between the calculations and the gui

    Args:
        String: Height to be converted and calculated with
        String: Weight to be converted and calculated with

    Returns:
        None

    Raises:
        None
    """
    if height.get() != '' and weight.get() != '':
        text = ""
        text = display_results(calculate_bmi(convert_height(height.get()), convert_weight(weight.get())), text)
    elif scrolledtext is None:
        add_textbox("You must enter a height and weight to proceed")
    else:
        scrolledtext.insert(INSERT, "\nYou must enter and height and weight to proceed")


def convert_weight(weight_lbs):
    """Converts weight from pounds to kilograms

    Args:
        float: weight in lbs

    Returns:
        float: weight in kg

    """
    try:
        conversion_rate = 2.2046
        weight_kg = float(weight_lbs) / conversion_rate
    except (ValueError, TypeError):
        scrolledtext.insert(INSERT, "\nError: improper weight input")
    return weight_kg


def convert_height(height_ft):
    """Converts height from a feet and inches string to meters

    Args:
        string: height in feet

    Returns:
        float: height in meters

    Raises:
        ValueError if height is improperly formatted
    """
    try:
        feet_to_inches = 12
        in_to_cm = 2.54
        cm_to_meters = 100
        height_list = height_ft.split("/")
        height_one = height_list[0]
        height_two = height_list[1]
        height_in = (int(height_one) * feet_to_inches) + int(height_two)
        height_meters = (height_in * in_to_cm) / cm_to_meters
        return height_meters
    except (ValueError, IndexError):
        scrolledtext.insert(INSERT, "\nError: height could not be converted to integer values")
    if height_ft.find("/") != 1:
        raise ValueError("\nHeight is not properly formatted")


def calculate_bmi(height, weight):
    """Calculates bmi from height and weight

    Args:
        float: height
        float: weight

    Returns:
        float: body mass index(bmi)

    Raises:
        TypeError if weight or height is not float
        ValueError if weight or height is negative

    """
    try:
        bmi = weight / (height**2)
        return bmi
    except TypeError:
        scrolledtext.insert(INSERT, "\nTypeError: Weight or height is not a float variable")
    if weight < 0 or height < 0:
        raise ValueError()
        scrolledtext.insert(INSERT, "\nHeight and weight must be positive; height was %d weight was %d" % (height, weight))


def display_results(bmi, text):
    """Displays bmi and a key for bmi ranges

    Args:
        float: body mass index
        text: what has already been printed to the textbox

    Returns:
        None

    Raises:
        None

    """
    if bmi > 18.5 and bmi <= 25:
        text = "\nYour bmi is in normal range at %.2f" % bmi
    elif bmi > 25 and bmi <= 30:
        text = "\nYou are overweight with a bmi of %.2f" % bmi
    elif bmi > 30:
        text = "\nYou are obese with a bmi of %.2f" % bmi
    else:
        text = "\nYou are underweight with a bmi of %.2f" % bmi
    scrolledtext.insert(INSERT, text)
    return text


def main():
    """Runs main program logic"""
    try:
        create_root()
        root.mainloop()
    except:
        scrolledtext.insert(INSERT, "Unexpected error")
        scrolledtext.insert(INSERT, "Error:", sys.exc_info()[1])
        scrolledtext.insert(INSERT, "File: ", sys.exc_info()[2].tb_frame.f_code.co_filename)
        scrolledtext.insert(INSERT, "Line: ", sys.exc_info()[2].tb_lineno)


main()