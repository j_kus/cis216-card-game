import unittest
from Enums.Suit import*


class SuitTestCase(unittest.TestCase):
    """Test Suit class."""

    def setUp(self):
        pass

    def test_value_for_HEARTS(self):
        self.assertEqual(Suit.HEARTS.value, 4, "HEARTS value is not 4")

    def test_value_for_SPADES(self):
        self.assertEqual(Suit.SPADES.value, 3, "SPADES value is not 3.")

    def test_value_for_DIAMONDS(self):
        self.assertEqual(Suit.DIAMONDS.value, 2, "DIAMONDS value is not 2.")

    def test_value_for_CLUBS(self):
        self.assertEqual(Suit.CLUBS.value, 1, "CLUBS value is not 1.")

    def test_name_for_HEARTS(self):
        self.assertEqual(Suit.HEARTS.name, "HEARTS", "HEARTS name not HEARTS.")

    def test_name_for_SPADES(self):
        self.assertEqual(Suit.SPADES.name, "SPADES", "SPADES name not SPADES.")

    def test_name_for_DIAMONDS(self):
        self.assertEqual(Suit.DIAMONDS.name, "DIAMONDS", "DIAMONDS name not DIAMONDS.")

    def test_name_for_CLUBS(self):
        self.assertEqual(Suit.CLUBS.name, "CLUBS", "CLUBS name is not CLUBS.")

if __name__ == '__main__':
    unittest.main(exit=False)
