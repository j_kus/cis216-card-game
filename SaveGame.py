from Game import *
from Rules import *
import shelve

def savegame(gameobj, rulesobj, filename="savefile"):
    """METHOD: Saves the entire game to a file.

        PARAMETERS:
            gameobj (Game object): Game object to be saved.
            rulesobj (Rules object): Rules object to be saved.
            filename (string): Name of the file.
    """
    savedata = shelve.open(filename)
    savedata["gamedata"] = gameobj
    savedata["rulesdata"] = rulesobj
    savedata.close()

def loadgame(filename="savefile"):
    """METHOD: Loads the game from a file.

        PARAMETERS:
            filename (string): Name of the file.

        RETURNS:
            Game object: Saved Game data.
    """
    savedata = shelve.open(filename)
    gamedata = savedata["gamedata"]
    savedata.close()
    return gamedata

def loadrules(filename="savefile"):
    """METHOD: Loads the game rules from a file.

        PARAMETERS:
            filename (string): Name of the file.
               
        RETURNS:
            Rules object: Saved Rules data.
    """
    savedata = shelve.open(filename)
    rulesdata = savedata["rulesdata"]
    savedata.close()
    return rulesdata

testgame = Game()
testrules = Rules()

savegame(testgame, testrules, "stuff")

newgame = loadgame("stuff")
newrules = loadrules("stuff")

print(newgame)
print(newgame.ace_rank)
print(str(newgame))
print(newgame.deck)
print(newrules)
