from Enums.Suit import *
from Enums.Rank import *
from Hand import *
from Deck import *
from Card import *
from Enums.Visibility import *

def main():
    """Adds 10 cards to 4 hands, and tests to ensure cards can be played and a field_of_play
    can be printed"""
    
    hand0 = Hand(sort = Sort.RANKTHENSUITA)
    hand1 = Hand()
    hand2 = Hand()
    hand3 = Hand()
    hands = [hand0, hand1, hand2, hand3]
    deck = Deck()
    deck.shuffle()
    count = 0
    for hand in hands:
        count += 1
        while hand.card_count <= 10:
            card = deck.deal()
            hand.add_card(card)
        print("hand %s : %s" % (str(count), hand))
    f_o_p = test_play_cards(hand1)
    test_print_card_deck_hand(hand3, f_o_p)

def test_play_cards(hand):
    print(hand)
    choice = int(input("Enter the number of the card you want to play: "))
    field_of_play = Discard_Pile(visibility = Visibility.VISIBLE)
    field_of_play.add_card(hand.play_card(choice))
    print(field_of_play)
    return field_of_play
    
def test_print_card_deck_hand(hand, field_of_play):
    print("hand: %s" % hand)
    print("Field of play: %s" % field_of_play)
    for card in hand.cards:
        print(card)
    
        
if __name__ == '__main__':
    main()