""" Runs all unit tests in the current folder. Files must begin with test (lower case).

# References:
#   https://stackoverflow.com/questions/3295386/python-unittest-and-discovery
"""

import unittest

loader = unittest.TestLoader()
tests = loader.discover('.')
testRunner = unittest.runner.TextTestRunner()
testRunner.run(tests)