import unittest
from Enums.Visibility import *


class VisibilityTestCase(unittest.TestCase):
    """Test Suit class."""

    def setUp(self):
        pass

    def test_value_for_VISIBLE(self):
        self.assertEqual(Visibility.VISIBLE.value, 1, "VISIBLE value is not 1")

    def test_value_for_INVISIBLE(self):
        self.assertEqual(Visibility.INVISIBLE.value, 2, "INVISIBLE value is not 2.")

    def test_value_for_TOPVISIBLE(self):
        self.assertEqual(Visibility.TOPVISIBLE.value, 3, "TOPVISIBLE value is not 3.")

    def test_name_for_VISIBLE(self):
        self.assertEqual(Visibility.VISIBLE.name, "VISIBLE", "VISIBLE name is not VISIBLE")

    def test_name_for_INVISIBLE(self):
        self.assertEqual(Visibility.INVISIBLE.name, "INVISIBLE", "INVISIBLE name is not INVISIBLE.")

    def test_name_for_TOPVISIBLE(self):
        self.assertEqual(Visibility.TOPVISIBLE.name, "TOPVISIBLE", "TOPVISIBLE name is not TOPVISIBLE.")

if __name__ == '__main__':
    unittest.main(exit=False)
