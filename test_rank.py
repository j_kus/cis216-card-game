import unittest
from Enums.Rank import*


class RankTestCase(unittest.TestCase):
    """Test Rank class."""

    def setUp(self):
        pass

    def test_value_for_ACELOW(self):
        self.assertEqual(Rank.ACELOW.value, 1, "ACELOW value is not 1")

    def test_value_for_TWO(self):
        self.assertEqual(Rank.TWO.value, 2, "TWO value is not 2")

    def test_value_for_THREE(self):
        self.assertEqual(Rank.THREE.value, 3, "THREE value is not 3")

    def test_value_for_FOUR(self):
        self.assertEqual(Rank.FOUR.value, 4, "FOUR value is not 4")

    def test_value_for_FIVE(self):
        self.assertEqual(Rank.FIVE.value, 5, "FIVE value is not 5")

    def test_value_for_SIX(self):
        self.assertEqual(Rank.SIX.value, 6, "SIX value is not 6")

    def test_value_for_SEVEN(self):
        self.assertEqual(Rank.SEVEN.value, 7, "SEVEN value is not 7")

    def test_value_for_EIGHT(self):
        self.assertEqual(Rank.EIGHT.value, 8, "EIGHT value is not 8")

    def test_value_for_NINE(self):
        self.assertEqual(Rank.NINE.value, 9, "NINE value is not 9")

    def test_value_for_TEN(self):
        self.assertEqual(Rank.TEN.value, 10, "TEN value is not 10")

    def test_value_for_JACK(self):
        self.assertEqual(Rank.JACK.value, 11, "TWO value is not 11")

    def test_value_for_QUEEN(self):
        self.assertEqual(Rank.QUEEN.value, 12, "TWO value is not 12")

    def test_value_for_KING(self):
        self.assertEqual(Rank.KING.value, 13, "KING value is not 13")

    def test_value_for_ACEHIGH(self):
        self.assertEqual(Rank.ACEHIGH.value, 14, "ACEHIGH value is not 14")

    def test_name_for_ACELOW(self):
        self.assertEqual(Rank.ACELOW.name, "ACELOW", "ACELOW name not ACELOW")

    def test_name_for_TWO(self):
        self.assertEqual(Rank.TWO.name, "TWO", "TWO value is not TWO")

    def test_name_for_THREE(self):
        self.assertEqual(Rank.THREE.name, "THREE", "THREE value is not THREE")

    def test_name_for_FOUR(self):
        self.assertEqual(Rank.FOUR.name, "FOUR", "FOUR value is not FOUR")

    def test_name_for_FIVE(self):
        self.assertEqual(Rank.FIVE.name, "FIVE", "FIVE value is not FIVE")

    def test_name_for_SIX(self):
        self.assertEqual(Rank.SIX.name, "SIX", "SIX value is not SIX")

    def test_name_for_SEVEN(self):
        self.assertEqual(Rank.SEVEN.name, "SEVEN", "SEVEN value is not 7")

    def test_name_for_EIGHT(self):
        self.assertEqual(Rank.EIGHT.name, "EIGHT", "EIGHT value is not 8")

    def test_name_for_NINE(self):
        self.assertEqual(Rank.NINE.name, "NINE", "NINE value is not 9")

    def test_name_for_TEN(self):
        self.assertEqual(Rank.TEN.name, "TEN", "TEN value is not 10")

    def test_name_for_JACK(self):
        self.assertEqual(Rank.JACK.name, "JACK", "TWO value is not 11")

    def test_name_for_QUEEN(self):
        self.assertEqual(Rank.QUEEN.name, "QUEEN", "TWO value is not 12")

    def test_name_for_KING(self):
        self.assertEqual(Rank.KING.name, "KING", "KING value is not 13")

    def test_name_for_ACEHIGH(self):
        self.assertEqual(Rank.ACEHIGH.name, "ACEHIGH", "ACEHIGH value not 14")

if __name__ == '__main__':
    unittest.main(exit=False)
