from flask import Flask
from flask import request
from Errors import *
from Game import *
from Enums.Visibility import *
from Enums.Rank import *
from Enums.Sort_Methods import *
from Build import *

app = Flask(__name__, static_folder='.', root_path='/home/runner/HTML')

@app.route('/', methods=['GET', 'POST'])
def root():
    """METHOD: Creates the root method for the flask module"""
    try:
        if request.method == "GET":
            return app.send_static_file('./instructions.html')
        elif request.method == "POST":
            if 'index' in request.form:
                game = HTML_Builder.start_game(int(request.form['index']))
                string = HTML_Builder.build_select(game)
                return string
            elif 'instructions' in request.form:
                return app.send_static_file('./index.html')

    except:
        Error.log_error(sys.exc_info()[1], "root()")
        string = ""
        string += str(sys.exc_info()[1])
        string += str(sys.exc_info()[2])
        string += str(sys.exc_info()[0])
        return string


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='3000', debug = True)
