"""Defines suit class for Hearts, Spades, Diamonds, and Clubs."""

from Errors import *
from enum import Enum


class Suit(Enum):
    HEARTS = 4
    SPADES = 3
    DIAMONDS = 2
    CLUBS = 1

    def __str__(self):
        """ Returns suit.

        Returns: (string) suit.

        """

        if self.name == "HEARTS":
            return "♡"
        if self.name == "SPADES":
            return "♠"
        if self.name == "DIAMONDS":
            return "♢"
        if self.name == "CLUBS":
            return "♣"
        else:
            Error.log_error(ValueError("Unexpected suit:" + self.name), "Suit()")

if __name__ == "__main__":

    for suit in Suit:
        print(suit)
