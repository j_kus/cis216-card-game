"""Defines Visibility class for visible, invisible, and topvisible."""

from Errors import *
from enum import Enum


class Visibility(Enum):
    VISIBLE = 1
    INVISIBLE = 2
    TOPVISIBLE = 3

    def __str__(self):
        """ Returns visible, invisible, and topvisible.

        Returns: (string) visible, invisible, and topvisible.
        """

        try:
            if self.name == "VISIBLE":
                return "V"
            if self.name == "INVISIBLE":
                return "I"
            if self.name == "TOPVISIBLE":
                return "T"
        except:
            Error.log_error(sys.exc_info()[1], "Visibility._str_()")

if __name__ == "__main__":
    for visible in Visibility:
        print(visible)
