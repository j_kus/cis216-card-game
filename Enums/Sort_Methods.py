"""Defines Sort methods class which determines how to sort cards in a hand."""

from enum import Enum
import sys
from Errors import *

class Sort(Enum):
    RANKTHENSUITD = 1
    SUITTHENRANKD = 2
    RANKTHENSUITA = 3
    SUITTHENRANKA = 4
       
    def __str__(self):
        """ Returns string for sort method.

        Returns: (string) ascending/descending rank then suit or suit then rank 
        """
        
        if self.name == "RANKTHENSUITD":
            return "Descending Rank then Suit"
        if self.name == "SUITTHENRANKD":
            return "Descending Suit then Rank"
        if self.name == "RANKTHENSUITA":
            return "Ascending Rank then Suit"
        if self.name == "SUITTHENRANKA":
            return "Ascending Suit then Rank"
        else:
            Error.log_error(ValueError("Unexpected sort method:" + self.name), "Sort_Methods()")

if __name__ == "__main__":
    for sort in Sort:
        print(sort)

