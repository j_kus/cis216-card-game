"""Author: Joanna Kus
 This class includes unit tests for the Card class.
 Used with Card.py
"""

import unittest
from Card import *
from Enums.Rank import *
from Enums.Suit import *

class Card_TestCase(unittest.TestCase):
    """CLASS: Units tests the Card class.

    METHODS:
        >> setUp(self)
            Sets up the testing suite.

        >> test_constructor_suit_none(self)
            Tests if the constructor defaults the suit parameter to None.

        >> test_constructor_rank_none(self)
            Tests if the constructor defaults the rank parameter to None.
            
        >> test_constructor_top_img_none(self)
            Tests if the constructor defaults the top_img parameter to None.
            
        >> test_constructor_side_img_none(self)
            Tests if the constructor defaults the side_img parameter to None.

        >> test_constructor_full_img_none(self)
            Tests if the constructor defaults the full_img parameter to None.
            
        >> test_str(self)
            Tests if the __str__ function prints the correct card rank and suit when the card is printed.

        >> test_compare_cards_rank_lesser(self)
            Tests if the compare_cards_by_rank function returns -1 for a card lesser in
            rank than the first object called.
            
        >> test_compare_cards_rank_equal(self)
            Tests if the compare_cards_by_rank function returns 0 for a card equal in
            rank than the first object called.
                       
        >> test_compare_cards_rank_greater(self)
            Tests if the compare_cards_by_rank function returns 1 for a card greater in
            rank than the first object called.
          
        >> test_compare_cards_suit_lesser(self)
            Tests if the compare_cards_by_suit function returns -1 for a card lesser in
            suit than the first object called.
              
        >> test_compare_cards_suit_equal(self)
            Tests if the compare_cards_by_suit function returns 0 for a card equal in
            suit than the first object called.
              
        >> test_compare_cards_suit_greater(self)
            Tests if the compare_cards_by_suit function returns 1 for a card greater in
            suit than the first object called.
              
        >> test_compare_cards_lesser(self)
            Tests if the compare_cards_by_suit function returns -1 for a card greater
            than the first object called.
              
        >> test_compare_cards_equal(self)
            Tests if the compare_cards_by_suit function returns 0 for a card equal than
            the first object called.
                
        >> test_compare_cards_greater(self)
            Tests if the compare_cards_by_suit function returns 1 for a card greater
            than the first object called.
                 
        >> test_is_same_as_true(self)
            Tests if the is_same_as function returns True when comparing cards of the
            same value.
               
        >> test_is_same_as_false(self)
            Tests if the is_same_as function returns False when comparing cards of
            differing values.
    """

    def setUp(self):
        """METHOD: Sets up the testing suite.

            >> setUp(self)
        """
        pass

    def test_constructor_suit_none(self):
        """METHOD: Tests if the constructor defaults the suit parameter to None.

            >> test_constructor_suit_none(self)

            RAISES:
                AssertionError: Suit is not equal to None.
        """
        newcard = Card()
        self.assertEqual(newcard.suit, None, "Suit not initialized to None in Cards constructor.")

    def test_constructor_rank_none(self):
        """METHOD: Tests if the constructor defaults the rank parameter to None.

            >> test_constructor_rank_none(self)

            RAISES:
                AssertionError: Rank is not equal to None.
        """
        newcard = Card()
        self.assertEqual(newcard.rank, None, "Rank not initialized to None in Cards constructor.")

    def test_constructor_top_img_none(self):
        """METHOD: Tests if the constructor defaults the top_img parameter to None.

            >> test_constructor_top_img_none(self)

            RAISES:
                AssertionError: top_img is not equal to None.
        """
        newcard = Card()
        self.assertEqual(newcard.top_img, None, "top_img not initialized to None in Cards constructor.")

    def test_constructor_side_img_none(self):
        """METHOD: Tests if the constructor defaults the side_img parameter to None.

            >> test_constructor_side_img_none(self)

            RAISES:
                AssertionError: side_img is not equal to None.
        """
        newcard = Card()
        self.assertEqual(newcard.side_img, None, "side_img not initialized to None in Cards constructor.")
        
    def test_constructor_full_img_none(self):
        """METHOD: Tests if the constructor defaults the full_img parameter to None.

            >> test_constructor_full_img_none(self)

            RAISES:
                AssertionError: full_img is not equal to None.
        """
        newcard = Card()
        self.assertEqual(newcard.full_img, None, "full_img not initialized to None in Cards constructor.")

    def test_str(self):
        """METHOD: Tests if the __str__ function prints the correct card rank and suit when the card is printed.
        
            >> test_str(self)
            
            RAISES:
                AssertionError: Incorrect card rank and suit printed.
        """
        newcard = Card(rank=Rank.FOUR, suit=Suit.CLUBS)
        self.assertEqual(str(newcard), "4\u2663", "Incorrect card rank and suit printed.")

    def test_compare_cards_rank_lesser(self):
        """METHOD: Tests if the compare_cards_by_rank function returns -1 for a
                   card lesser in rank than the first object called.
                   
            >> test_compare_cards_rank_lesser(self)

            RAISES:
                AssertionError: Function does not return -1.
        """
        newcard = Card(rank=Rank.TWO)
        othercard = Card(rank=Rank.FOUR)
        self.assertEqual(newcard.compare_cards_by_rank(othercard), -1, "Function did not return -1.")

    def test_compare_cards_rank_equal(self):
        """METHOD: Tests if the compare_cards_by_rank function returns 0 for a
                   card equal in rank than the first object called.
                   
            >> test_compare_cards_rank_equal(self)

            RAISES:
                AssertionError: Function does not return 0.
        """
        newcard = Card(rank=Rank.FOUR)
        othercard = Card(rank=Rank.FOUR)
        self.assertEqual(newcard.compare_cards_by_rank(othercard), 0, "Function did not return 0.")

    def test_compare_cards_rank_greater(self):
        """METHOD: Tests if the compare_cards_by_rank function returns 1 for a
                   card greater in rank than the first object called.
                   
            >> test_compare_cards_rank_greater(self)

            RAISES:
                AssertionError: Function does not return 1.
        """
        newcard = Card(rank=Rank.FIVE)
        othercard = Card(rank=Rank.FOUR)
        self.assertEqual(newcard.compare_cards_by_rank(othercard), 1, "Function did not return 1.")

    def test_compare_cards_suit_lesser(self):
        """METHOD: Tests if the compare_cards_by_suit function returns -1 for a
                   card lesser in suit than the first object called.
                   
            >> test_compare_cards_suit_lesser(self)

            RAISES:
                AssertionError: Function does not return -1.
        """
        newcard = Card(suit=Suit.CLUBS)
        othercard = Card(suit=Suit.DIAMONDS)
        self.assertEqual(newcard.compare_cards_by_suit(othercard), -1, "Function did not return -1.")

    def test_compare_cards_suit_equal(self):
        """METHOD: Tests if the compare_cards_by_suit function returns 0 for a
                   card equal in suit than the first object called.
                   
            >> test_compare_cards_suit_equal(self)

            RAISES:
                AssertionError: Function does not return 0.
        """
        newcard = Card(suit=Suit.DIAMONDS)
        othercard = Card(suit=Suit.DIAMONDS)
        self.assertEqual(newcard.compare_cards_by_suit(othercard), 0, "Function did not return 0.")

    def test_compare_cards_suit_greater(self):
        """METHOD: Tests if the compare_cards_by_suit function returns 1 for a
                   card greater in suit than the first object called.
                   
            >> test_compare_cards_suit_greater(self)

            RAISES:
                AssertionError: Function does not return 1.
        """
        newcard = Card(suit=Suit.HEARTS)
        othercard = Card(suit=Suit.DIAMONDS)
        self.assertEqual(newcard.compare_cards_by_suit(othercard), 1, "Function did not return 1.")

    def test_compare_cards_lesser(self):
        """METHOD: Tests if the compare_cards_by_suit function returns -1 for a
                   card greater than the first object called.
                   
            >> test_compare_cards_lesser(self)

            RAISES:
                AssertionError: Function does not return -1.
        """
        newcard = Card(rank=Rank.TWO, suit=Suit.CLUBS)
        othercard = Card(rank=Rank.TWO, suit=Suit.DIAMONDS)
        self.assertEqual(newcard.compare_cards_by_suit(othercard), -1, "Function did not return -1.")

    def test_compare_cards_equal(self):
        """METHOD: Tests if the compare_cards_by_suit function returns 0 for a
                   card equal than the first object called.
                   
            >> test_compare_cards_equal(self)

            RAISES:
                AssertionError: Function does not return 0.
        """
        newcard = Card(rank=Rank.TWO, suit=Suit.DIAMONDS)
        othercard = Card(rank=Rank.TWO, suit=Suit.DIAMONDS)
        self.assertEqual(newcard.compare_cards_by_suit(othercard), 0, "Function did not return 0.")

    def test_compare_cards_greater(self):
        """METHOD: Tests if the compare_cards_by_suit function returns 1 for a
                   card greater than the first object called.
                   
            >> test_compare_cards_greater(self)

            RAISES:
                AssertionError: Function does not return 1.
        """
        newcard = Card(rank=Rank.THREE, suit=Suit.HEARTS)
        othercard = Card(rank=Rank.TWO, suit=Suit.DIAMONDS)
        self.assertEqual(newcard.compare_cards_by_suit(othercard), 1, "Function did not return 1.")

    def test_is_same_as_true(self):
        """METHOD: Tests if the is_same_as function returns True when comparing
                   cards of the same value.
                   
            >> test_is_same_as_true(self)

            RAISES:
                AssertionError: Function does not return True.
        """
        newcard = Card(rank=Rank.TWO, suit=Suit.CLUBS)
        othercard = Card(rank=Rank.TWO, suit=Suit.CLUBS)
        self.assertEqual(newcard.is_same_as(othercard), True, "Function did not return True.")

    def test_is_same_as_false(self):
        """METHOD: Tests if the is_same_as function returns False when comparing
                   cards of differing values.
                   
            >> test_is_same_as_false(self)

            RAISES:
                AssertionError: Function does not return False.
        """
        newcard = Card(rank=Rank.TWO, suit=Suit.CLUBS)
        othercard = Card(rank=Rank.FIVE, suit=Suit.HEARTS)
        self.assertEqual(newcard.is_same_as(othercard), False, "Function did not return False.")

if __name__ == "__main__":
    unittest.main()
