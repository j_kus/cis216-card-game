## Quick overview of who's doing what.
* Tristan Blus - Hand superclass (Hand.py), game class functions(Game.py), Rules unit testing(RulesTest.py), Game unit testing(GameTest.py), IntegrationTest.py, Main.py, Error.py, all error logs, Sort_Methods.py, Build.py, Flask.py   
* Dunya Kerkuk - Rank class (Rank.py), Rank unittest (RankTest.py), Suit class (Suit.py), Suit unittest (SuitTest.py), visibility class (Visibility.py), Visibility unittest (VisibilityTest.py), Rules class (Rules.py), Index.html, Message.html, Deck.html, Instructions.html, gofish.css, Flask.py
* Joanna Kus - Card class (Card.py) and game class (Game.py)
* Alexander Lazarov - Deck class (Deck.py) and GUI class (GUI.py

**Check your UMLs.**

## UML

### Card
* \+ suit: string {read only}
* \+ rank: string {read only}
* \+ card_image: string {read only}
* \+ isvisible: Enum (Visibility)
----
* \+ compare_cards_by_rank(other_card: Card)
* \+ compare_cards_by_suit(other_card: Card)
* \+ compare_cards(other_card: Card)
* \+ is_same_as(other_card: Card)

### Deck
* \+  deck_count: int {read only}
* \+  deck: array
----
* \+ build(deck: array)
* \+ shuffle(deck: array)
* \+ deal(deck)
* \+ print_deck(deck)
* \-validate_count(deck)



### Hand
* \+ card_count: int {read only}
* \+ sort_type : Sort
* \+ cards: array
* \+ isvisible : Visibility
----
* \+ add_card(card: card)
* \+ remove_card(card: card = None, index: int = None)
* \+ play_card(card: card)
* \+ evaluate_hand(cards: array)
* \+ visi_override()
* \+ count_rank(rank: Rank)
* \+ count_suit(suit: Suit)
* \+ find_and_remove(request: Suir or  Rank)
* \+ compare_hand(hand: hand)
* \- validate_card(card: card)
* \- sort_cards(cards: array)

### GUI
* \+ player_select_window
* \+ init_window
* \+ set_player_count
* \+ submit_request
* \+ reveal_button
* \+ player_turn
* \+ player_score_title
* \+ player_score
* \+ play_game
* \+ rank_button_state
* \+ submit_rank
* \+ submit_player
* \+ choose_hand
* \+ reveal
* \+ quit


### Rank
* \+ ace
* \+ one
* \+ two
* \+ three
* \+ four
* \+ five
* \+ six
* \+ seven
* \+ eight
* \+ nine
* \+ ten
* \+ jack
* \+ queen
* \+ king
----
* \+ get_rank

### Suit
* \+ spades
* \+ hearts
* \+ clubs
* \+ diamonds
----
* \+ get_suit

### Game
* \+ sort_method
* \+ log_file_name
* \+ empty_hands
* \+ active_hand
* \+ init_hand_size
* \+ hand_count
* \+ max_hannd_size
* \+ hands
* \+ ace_rank
* \+ discard_type
* \+ deck
----
* \+ control_players
* \+ choose_hand

### Rules
* \+ points
* \+ books
----
* \+ card_request
* \+ player_answer
* \+ book_check
* \+ count_request
* \+ check_deck
* \+ play_game

### HTML
*  index is Get
*  message
*  deck is Post
*  Instructions is Get
*  Selectplayer is Post

### CSS
*  gofish

### Flask

### Run the game using GUI version:
*  Download GUI branch files to IDE.
*  Open GUI.py file.
*  Run GUI.py file.
*  A window will open up to choose the number of players.
*  After choosing the number of players,  a new window will open up to start the game.
*  Select the player you want to ask for a card then play.

### Run the game using HTML version:
*  Download HTML branch files to IDE.
*  Open Flask.py file.
*  Run Flask.py file.
*  An HTML page will open up displaying the game instructions.
*  Click the Start button.
*  Enter the number of players then click submit.
*  View the number of players in the game and their scores.
