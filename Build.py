from Game import *
from Enums.Visibility import *
from Enums.Rank import *
from Enums.Sort_Methods import *

class HTML_Builder(object):
        
    @staticmethod
    def start_game(count):
        """METHOD Instantiates a game object based on a hand count passed to it

        PARAMETERS
            @count (Int): The integer count of hands in the game

        RETURNS
            @game (Game): The game object to be used elsewhere
        """
        game = Game(hand_size = 5,
            hand_count = count, 
            max_hand_size = 52, 
            discard_type = Visibility.INVISIBLE, 
            ace_rank = Rank.ACELOW, 
            sort = Sort.RANKTHENSUITA)
        return game

    @staticmethod
    def build_select(game):
        """METHOD Generates the select player view for the html card game

        PARAMETERS
            @game (Game): the game object currently instantiated
        
        RETURNS
            @start (string): The html string to be printed to browser
        """
        start = """ <!DOCTYPE html>
                    <html lang="en">
                    <head>
                    <title>GO FISH GAME</title>
                    <link rel="icon" href="fish.png"></link>
                    <meta charset="utf-8">
                    <meta name="description" content="In this page the user should select the number of player to play go fish game.">
                    <meta name="author" content="Dunya Kerkuk">
                    <link rel="stylesheet" type="text/css" href="gofish.css"/>
                    </head>
                    <body>
                    	<div id="wrapper">
                    	<header>
                    		<h1>GO FISH GAME</h1>
                    	</header>
                    	<main>
                    	<hr>
                    	<h3 class="text"><b>Players scores</b><h3>
                    	<div class="container3">"""
        for hand in game.hands:
            temp = """<div class="scores-container"><b>%s : %s</b></div>""" % (hand, game.hands[hand][1])
            start += temp
            
        temp = """</div><br>
            	<br>
            	<br>
            	<br>
            	<h3 class="text">Select a player to ask for a card</h3><br>
            	<div class="container2">
                <form name="form" method="post" action"">"""
	    
        start += temp
	    
        for hand in game.hands:
	        temp = """<button type="submit" class="button" name = 'hand' Id="p1"><b>%s</b></button>""" % hand
	        start += temp
	    
	    
        start += """	</form>
                        </div>
                    	<br>
                    	<br>
                    	</main>
                    	<footer>
                    		<br>
                    		<hr>
                    		Copyright &copy; 2018 GO FISH
                    		<br>
                    		<br>
                    	</footer>
                    </body>
                    <br>
                    </html>
                    """
                      
        return start
	
    @staticmethod
    def build_deck(game):
        """METHOD Generates the deck view for the html card game

        PARAMETERS
            @game (Game): the game object currently instantiated
        
        RETURNS
            @start (string): The html string to be printed to browser
        """
        start = """
        <!DOCTYPE html>
        <html lang="en">
        <head>
        <title>GO FISH GAME</title>
        <link rel="icon" href="fish.png"></link>
        <meta charset="utf-8">
        <meta name="description" content="In this page the user should select the number of player to play go fish game.">
        <meta name="author" content="Dunya Kerkuk">
        <link rel="stylesheet" type="text/css" href="gofish.css"/>
        </head>
        <body>
        <div id="wrapper">
        <header>
        <h1>GO FISH GAME</h1>
        </header>
        <main>
        <hr>
        <div class="container">
        <form name="form" method="post">
        <input name="name" placeholder="Enter card you ask" type="text">
        <input type="submit" value="Ask">
        </form>
        </div>

        <div class="deck-container">Deck</div><br>
        <div class="container4">
        """
        game.active_hand = hands['hands0']
        for card in game.active_hand.cards:
            temp = """<div class = "active-container"><img src=\""""
            temp += str(card.full_img)
            temp += """\"width="auto" max-height="100%"></div>\n"""
            start += temp
        
        cont = """  </div>\n
	                <br>\n\n
	
                    <div class="container4">
	              		<div class="player-container"></div>
		                <div class="player-container"></div>
                		<div class="player-container"></div>
                		<div class="player-container"></div>
                    </div>
                	<br>
                	</main>
                	<footer>
                		<br>
                		<hr>
                		Copyright &copy; 2018 GO FISH
                		<br>
                		<br>
                	</footer>
                </body>
                <br>
                </html>

	           """
        start += cont
        return start
        
        

def main():
    pass

main()